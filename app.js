var errorEvent = debounce(restartConnection, 2000);
console.log("Stream status reloaded");
function onError(error) {
  console.log(`Error: ${error}`);
  errorEvent();
}

function getSettings() {
  return browser.storage.sync.get("settings");
}

function openPage() {
  getSettings().then((result) => {
      browser.tabs.create({
        url: result.settings.redirect_url,
      });
  });
}
browser.browserAction.onClicked.addListener(openPage);
browser.browserAction.setIcon({"path" : "icons/off.png"});

var peerConnection = null;
function restartConnection() {
  if (peerConnection != null) {
    peerConnection.close();
  }
  getSettings().then((result) => {
    peerConnection = checkStreamStatus(result.settings.stream_url, result.settings.stream_key, null) //stun:stun.l.google.com:19302
  });
}
restartConnection();
function checkStreamStatus(url, key, stun) {
  var STUN = {
      'urls': ["stun:stun.l.google.com:19302"]
  };

  var iceServers = {
    iceServers: [STUN],
  };
  const peerConnection = new RTCPeerConnection(iceServers);
  peerConnection.addTransceiver('audio', {direction: 'recvonly'});
  peerConnection.addTransceiver('video', {direction: 'inactive'});

  peerConnection.onicegatheringstatechange = (event) => {
    let connection = event.target;

    switch (connection.iceGatheringState) {
      case "gathering":
        console.log("Gathering")
        break;
      case "complete":
        console.log("Gathering complete")
        sendOffer(url, key, peerConnection.pendingLocalDescription)
        break;
    }
  };

  peerConnection.onsignalingstatechange = (event) => {
    console.log(event.target.signalingState)
  }

  peerConnection.setLocalDescription(null)
  peerConnection.createOffer().then((offer) => {
    console.log("Sest")
    console.log(offer.sdp)
  });

  return peerConnection
}

function sendOffer(url, key, offer) {
  console.log(offer.sdp)
  fetch(`${url}`, {
    method: 'POST',
    body: offer.sdp,
    headers: {
      Authorization: `Bearer ${key}`,
      'Content-Type': 'application/sdp'
    }
  }).then(r => {
    return r.text();
  }).then(answer => {
    peerConnection.setRemoteDescription({
      sdp: answer,
      type: 'answer'
    });
  }).catch(onError);
}


function debounce(func, timeout = 2000){
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args); 
    }, timeout);
  };
}

var streamOnlineEvent = debounce(() => streamOffline());
function streamOnline() {
  console.log("Stream online")
  browser.browserAction.setIcon({"path" : "icons/on.png"});
  streamOnlineEvent();
}

function streamOffline() {
  console.log("Steam offline")
  browser.browserAction.setIcon({"path" : "icons/off.png"});
  restartConnection();
}


var lastReceivedPackets = 0;
setInterval(() => getConnectionStats(), 1000);
function getConnectionStats() {
  if (peerConnection != null) {
    peerConnection.getStats(null).then((stats) => {
      stats.forEach((report) => {
        if (report.type === "inbound-rtp" && report.kind === "audio" && report.packetsReceived >= 0) {
          if (report.packetsReceived != lastReceivedPackets) {
            lastReceivedPackets = report.packetsReceived;
            streamOnline();
          }
        }
      });
    });
  }
}

