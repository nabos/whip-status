Firefox extension monitoring a video stream via the WHIP negociation protocol.  
A focus has been made on the low processing, the low data volume streaming, and making only as few network requests as possible.  

There is 3 settings:
- stream_url: the base url at which the negociation will happen
    example: https://<host>/api/whep
- stream_key: the stream key which is used by the sender
- redirect_url: the url on which you will be redirected on a click on the extension icon
