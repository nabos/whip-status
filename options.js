function saveOptions(e) {
  e.preventDefault();
  browser.storage.sync.set({
    settings: {
      stream_url: document.querySelector("#stream_url").value,
      stream_key: document.querySelector("#stream_key").value,
      redirect_url: document.querySelector("#redirect_url").value,
    }
  });
}

function restoreOptions() {
  function setCurrentChoice(result) {
    document.querySelector("#stream_url").value = result.settings.stream_url || "https://<stream_url>";
    document.querySelector("#stream_key").value = result.settings.stream_key || "<key>";
    document.querySelector("#redirect_url").value = result.settings.redirect_url || "https://<redirect_url>";
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  let getting = browser.storage.sync.get("settings");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);

